﻿using System;
using System.Collections.Generic;
using System.Linq;
using FlagNET.Usage.Flags;
using SimpleInjector;

namespace FlagNET.Usage
{
	internal class Program
	{
		#region Fields

		private readonly IFlag flag;

		#endregion

		#region Constructors

		public Program(IFlag flag)
		{
			this.flag = flag;
		}

		#endregion

		#region Properties

		#endregion

		#region Methods

		private void Run(string[] args)
		{
			Console.WriteLine("START");
			Console.WriteLine("---");

			// the simplest kind of flag is simply ON or OFF

			if (flag.IsOn<CoolThingFlag>())
			{
				Console.WriteLine("COOL");
			}
			else
			{
				Console.WriteLine("NOT SO COOL");
			}

			Console.WriteLine("---");

			// flags can take more complicated parameters though
			// but checking them is always a simple, boolean operation

			for (int i = 0; i < 10; i++)
			{
				if (flag.IsOn<SometimesFlag>())
				{
					Console.WriteLine("SOMETIMES " + i);
				}
			}

			Console.WriteLine("---");

			if (flag.IsOn<FancyThingFlag>())
			{
				Console.WriteLine("I'M SO FANCY");
			}

			Console.WriteLine("---");
			Console.WriteLine("END");
			Console.Read();
		}

		#endregion

		private static void Main(string[] args)
		{
			var container = new Container();

			container.Register<Program>();
			container.RegisterSingle<IFlag, Flag>();
			container.RegisterSingle<IFlagKeyProvider, FlagKeyProvider>();
			container.RegisterSingle<IFlagSettingProvider, FlagSettingProvider>();
			container.RegisterSingle<IFlagProvider, FlagProvider>();
			container.RegisterSingle<IFlagSettingValueParser, FlagSettingValueParser>();

			// we want to store settings in app.config
			container.RegisterSingle<IFlagSettingReader, AppConfigFlagSettingReader>();

			container.Verify();

			var program = container.GetInstance<Program>();

			program.Run(args);
		}
	}
}
