﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace FlagNET.Usage.Flags
{
	class SometimesFlag : FlagBase<double>
	{
		private readonly Random random;

		public SometimesFlag(Random random)
		{
			this.random = random;
		}

		protected override bool IsOn(double setting)
		{
			return random.NextDouble() < setting;
		}
	}
}
