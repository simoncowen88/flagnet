﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace FlagNET.Usage.Flags
{
	class FancyThingFlag : FlagBase<string[]>
	{
		public FancyThingFlag()
		{
		}

		protected override bool IsOn(string[] setting)
		{
			var user = Environment.UserName;
			return setting.Contains(user);
		}
	}
}
