﻿using System;
using System.Linq;
using SimpleInjector;

namespace FlagNET.Usage.Flags
{
	public class FlagProvider : IFlagProvider
	{
		private readonly Container container;

		public FlagProvider()
		{
			this.container = new Container();
			
			var flagsNamespace = typeof(FlagProvider).Namespace;
			var flagTypes = typeof(FlagProvider).Assembly
				.GetTypes()
				.Where(t => t.Namespace == flagsNamespace)
				.Where(t => !t.IsAbstract)
				.Where(t => typeof(FlagBase).IsAssignableFrom(t));

			foreach (var flagType in flagTypes)
			{
				container.RegisterSingle(flagType, flagType);
			}

			container.Register(() => new Random());

			container.Verify();
		}

		public TFlag Get<TFlag>()
			where TFlag : FlagBase
		{
			return container.GetInstance<TFlag>();
		}
	}
}