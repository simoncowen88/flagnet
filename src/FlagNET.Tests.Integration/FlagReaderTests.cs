﻿using System;
using NUnit.Framework;

namespace FlagNET.Tests.Integration
{
	[TestFixture]
	public class FlagReaderTests
	{
		#region Helper

		public abstract class TestBase
		{
			[SetUp]
			public virtual void SetUp()
			{
			}

			[TearDown]
			public virtual void TearDown()
			{
			}

			internal Flag GetTarget()
			{
				return new Flag(
					new FlagSettingProvider(new FlagKeyProvider(), new TestFlagSettingReader(), 
						new FlagSettingValueParser()),
					new ActivatorFlagProvider());
			}
		}

		private class TestFlagSettingReader : IFlagSettingReader
		{
			public bool TryRead(string key, out string setting)
			{
				if (key == "FlagNET.Tests.Integration.FlagReaderTests+TestFlag")
				{
					setting = "true";
					return true;
				}

				setting = null;
				return false;
			}
		}

		private class TestFlag : BoolFlag { }
		private class StupidFlag<T> : BoolFlag { }

		#endregion
		
		[TestFixture]
		public class IntegrationTests : TestBase
		{
			[Test]
			public void works_as_expected()
			{
				var target = GetTarget();
				var isOn = target.IsOn<TestFlag>();
				Assert.That(isOn, Is.True);
			}

			[Test]
			public void throws_when_flag_type_generic()
			{
				var target = GetTarget();

				Assert.Throws<Exception>(() => target.IsOn<StupidFlag<int>>());
			}
		}
	}
}
