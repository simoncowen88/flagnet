﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Linq.Expressions;

namespace FlagNET
{
	/// <summary>
	/// Provides methods for parsing setting strings.
	/// </summary>
	public interface IFlagSettingValueParser
	{
		/// <summary>
		/// Parses the specified setting value string to its typed equivalent.
		/// </summary>
		/// <typeparam name="T">The type of value to parse to.</typeparam>
		/// <param name="settingValueString">
		/// A string containing a value to convert.
		/// </param>
		/// <returns>
		/// A typed instance equivalent to the kang. contained in the string.
		/// </returns>
		T Parse<T>(string settingValueString);
	}

	/// <summary>
	/// Provides methods for parsing setting strings.
	/// This provider can parse T, T?, T[], and T?[] where T is a built-in type.
	/// </summary>
	public class FlagSettingValueParser : IFlagSettingValueParser
	{
		#region Fields

		private delegate object ParseDelegate(string s);
		private static readonly Dictionary<Type, ParseDelegate> parseFuncMap; 

		#endregion

		#region Constructors

		static FlagSettingValueParser()
		{
			parseFuncMap = new Dictionary<Type, ParseDelegate>
			{
				{typeof (bool), s => bool.Parse(s)},
				{typeof (byte), s => byte.Parse(s)},
				{typeof (short), s => short.Parse(s)},
				{typeof (int), s => int.Parse(s)},
				{typeof (long), s => long.Parse(s)},

				{typeof (float), s => float.Parse(s)},
				{typeof (double), s => double.Parse(s)},

				{typeof (decimal), s => decimal.Parse(s)},

				{typeof (string), s => s},

				{typeof (Guid), s => Guid.Parse(s)},
				{typeof (char), s => char.Parse(s)},
				{typeof (DateTime), s => DateTime.Parse(s)},
				{typeof (DateTimeOffset), s => DateTimeOffset.Parse(s)},
				{typeof (TimeSpan), s => TimeSpan.Parse(s)},

				{typeof (sbyte), s => sbyte.Parse(s)},
				{typeof (uint), s => uint.Parse(s)},
				{typeof (ulong), s => ulong.Parse(s)},
				{typeof (ushort), s => ushort.Parse(s)},
			};
		}

		#endregion

		#region Properties

		#endregion

		#region Methods

		/// <summary>
		/// Parses the specified setting value string to its typed equivalent.
		/// </summary>
		/// <typeparam name="T">The type of value to parse to.</typeparam>
		/// <param name="settingValueString">
		/// A string containing a value to convert.
		/// </param>
		/// <returns>
		/// A typed instance equivalent to the kang. contained in the string.
		/// </returns>
		public T Parse<T>(string settingValueString)
		{
			if (settingValueString == null)
				return default(T);

			var type = typeof (T);

			if (type.IsArray)
			{
				var parseArray = GetParseArray(type.GetElementType());
				var result = parseArray(settingValueString);

				Contract.Assume(result != null);

				return (T) result;
			}
			else
			{
				return ParseItem<T>(settingValueString);
			}
		}

		private static T[] ParseArray<T>(string settingValueString)
		{
			Contract.Requires(settingValueString != null);
			Contract.Ensures(Contract.Result<T[]>() != null);

			if (settingValueString == "")
				return new T[0];

			var settingValueStrings = settingValueString.Split(',');

			return settingValueStrings
				.Select(ParseItem<T>)
				.ToArray();
		}

		private static T ParseItem<T>(string settingValueString)
		{
			Contract.Requires(settingValueString != null);

			var type = typeof (T);
			var isNullable = type.IsGenericType &&
			                 type.GetGenericTypeDefinition() == typeof (Nullable<>);

			if (isNullable)
			{
				if (settingValueString == "null")
					return default (T);

				var underlyingType = Nullable.GetUnderlyingType(type);
				var parseBuiltIn = GetParseBuiltIn(underlyingType);
				var result = parseBuiltIn(settingValueString);

				Contract.Assume(result != null);

				return (T) result;
			}
			else
			{
				return ParseBuiltIn<T>(settingValueString);
			}
		}


		private static T ParseBuiltIn<T>(string settingValueString)
		{
			Contract.Requires(settingValueString != null);

			ParseDelegate parseFunc;

			if (!parseFuncMap.TryGetValue(typeof(T), out parseFunc))
			{
				throw new Exception(
					"Unable to parse the type of setting value. " +
					string.Format("Type was {0}. ", typeof(T).FullName) +
					"This provider can only parse T, T?, T[], and T?[] " +
					"where T is a built-in type. ");
			}

			Contract.Assume(parseFunc != null);

			object result;
			try
			{
				result = parseFunc(settingValueString);
			}
			catch (Exception exception)
			{
				throw new ArgumentException(
					"Could not parse the setting value string. " +
					string.Format("String was {0}. ", settingValueString) +
					string.Format("Type was {0}. ", typeof(T).FullName),
					"settingValueString",
					exception);
			}

			// either a string, in which case parseFunc is identity, and we have
			// not been passed null, or T : struct
			Contract.Assume(result != null);

			return (T)result;
		}

		private static Func<string, object> GetParseArray(Type type)
		{
			Contract.Ensures(Contract.Result<Func<string, object>>() != null);

			Func<string, bool[]> methodHandle = ParseArray<bool>;
			return GetSingleGenericMethod(type, methodHandle);
		}

		private static Func<string, object> GetParseBuiltIn(Type type)
		{
			Contract.Ensures(Contract.Result<Func<string, object>>() != null);

			Func<string, bool> methodHandle = ParseBuiltIn<bool>;
			return GetSingleGenericMethod(type, methodHandle);
		}

		private static Func<TParameter, object> GetSingleGenericMethod<TParameter, TReturn>(
			Type type,
			Func<TParameter, TReturn> methodHandle)
		{
			Contract.Requires(methodHandle != null);
			Contract.Ensures(Contract.Result<Func<TParameter, object>>() != null);

			var methodInfo = methodHandle.Method
				.GetGenericMethodDefinition()
				.MakeGenericMethod(type);

			var parameter = Expression.Parameter(typeof (TParameter));

			var callMethod = Expression.Call(methodInfo, parameter);
			Contract.Assume(callMethod != null);
			var convertReturnToObject = Expression.Convert(callMethod, typeof (object));
			Contract.Assume(convertReturnToObject != null);

			var lambda = Expression.Lambda<Func<TParameter, object>>(
				convertReturnToObject,
				parameter);

			var result = lambda.Compile();
			Contract.Assume(result != null);

			return result;
		}

		#endregion
	}
}
