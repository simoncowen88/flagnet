﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Diagnostics.Contracts;
using System.Linq;

namespace FlagNET
{
	/// <summary>
	/// Provides methods for getting strongly-typed settings for flags.
	/// </summary>
	[ContractClass(typeof (IFlagSettingProviderContract))]
	public interface IFlagSettingProvider
	{
		/// <summary>
		/// Gets the setting for the given TFlag.
		/// </summary>
		/// <typeparam name="T">The type of the setting.</typeparam>
		/// <typeparam name="TFlag">The type of flag.</typeparam>
		/// <returns>
		/// The requested setting.
		/// </returns>
		FlagSetting<T> Get<T, TFlag>()
			where TFlag : FlagBase<T>;
	}

	#region Contract class

	// ReSharper disable once InconsistentNaming
	[ContractClassFor(typeof(IFlagSettingProvider))]
	[ExcludeFromCodeCoverage]
	internal abstract class IFlagSettingProviderContract : IFlagSettingProvider
	{
		public FlagSetting<T> Get<T, TFlag>()
			where TFlag : FlagBase<T>
		{
			Contract.Ensures(Contract.Result<FlagSetting<T>>() != null);

			return default (FlagSetting<T>);
		}
	}

	#endregion
}
