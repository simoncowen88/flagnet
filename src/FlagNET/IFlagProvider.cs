﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Diagnostics.Contracts;
using System.Linq;

namespace FlagNET
{
	/// <summary>
	/// Provides methods for creating instances of specific flags.
	/// </summary>
	[ContractClass(typeof (IFlagProviderContract))]
	public interface IFlagProvider
	{
		/// <summary>
		/// Gets an instance of the given TFlag.
		/// </summary>
		/// <typeparam name="TFlag">The type of flag requested.</typeparam>
		/// <returns>
		/// The requested flag instance.
		/// </returns>
		TFlag Get<TFlag>()
			where TFlag : FlagBase;
	}

	#region Contract class

	// ReSharper disable once InconsistentNaming
	[ContractClassFor(typeof(IFlagProvider))]
	[ExcludeFromCodeCoverage]
	internal abstract class IFlagProviderContract : IFlagProvider
	{
		public TFlag Get<TFlag>()
			where TFlag : FlagBase
		{
			Contract.Ensures(Contract.Result<TFlag>() != null);

			return default(TFlag);
		}
	}

	#endregion
}
