﻿using System.Diagnostics.CodeAnalysis;
using System.Diagnostics.Contracts;

namespace FlagNET
{
	/// <summary>
	/// The base class for flag types.
	/// </summary>
	[Pure]
	[ContractClass(typeof(FlagBaseContract))]
	public abstract class FlagBase
	{
		// we want outside inheritors to have to go through FlagBase<T>
		internal FlagBase()
		{
		}

		internal abstract bool IsOn(IFlagSettingProvider flagSettingProvider);
	}

	#region Contract class

	// ReSharper disable once InconsistentNaming
	[ContractClassFor(typeof(FlagBase))]
	[ExcludeFromCodeCoverage]
	internal abstract class FlagBaseContract : FlagBase
	{
		internal override bool IsOn(IFlagSettingProvider flagSettingProvider)
		{
			Contract.Requires(flagSettingProvider != null);

			return default(bool);
		}
	}

	#endregion
}