﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Linq.Expressions;

namespace FlagNET
{
	/// <summary>
	/// The strongly-typed base class for flag types.
	/// </summary>
	[Pure]
	public abstract class FlagBase<T> : FlagBase
	{
		#region Fields

		private readonly Func<IFlagSettingProvider, FlagSetting<T>> getFlagSetting;

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="FlagBase{T}"/> class.
		/// </summary>
		protected FlagBase()
		{
			this.getFlagSetting = GetGetFlagSetting();
		}

		#endregion

		#region Properties

		#endregion

		#region Methods

		/// <summary>
		/// Determines whether this flag is enabled, given the specified setting.
		/// </summary>
		/// <param name="setting">The setting for the flag.</param>
		/// <returns>
		/// true, if the flag is enabled; otherwise, false.
		/// </returns>
		protected abstract bool IsOn(T setting);

		internal sealed override bool IsOn(IFlagSettingProvider flagSettingProvider)
		{
			Assertion.ArgumentNotNull(() => flagSettingProvider);

			var flagSetting = GetFlagSetting(flagSettingProvider);
			return flagSetting.Apply(IsOn);
		}

		private FlagSetting<T> GetFlagSetting(IFlagSettingProvider flagSettingProvider)
		{
			Contract.Requires(flagSettingProvider != null);
			Contract.Ensures(Contract.Result<FlagSetting<T>>() != null);

			var result = getFlagSetting(flagSettingProvider);

			// IFlagSettingProvider.Get`2 ensures this
			Contract.Assume(result != null);

			return (FlagSetting<T>) result;
		}

		private Func<IFlagSettingProvider, FlagSetting<T>> GetGetFlagSetting()
		{
			Contract.Ensures(Contract.Result<Func<IFlagSettingProvider, FlagSetting<T>>>() != null);

			// we want to do: return flagSettingProvider.Get<T, TFlag>();
			// however we don't statically know TFlag, but we are guaranteed
			// that our runtime type is derives from FlagBase<T>

			Func<IFlagSettingProvider, FlagSetting<T>> methodHandle =
				GetFlagSetting<FlagBase<T>>;
			var flagType = GetType();
			var methodInfo = methodHandle.Method
				.GetGenericMethodDefinition()
				.MakeGenericMethod(flagType);

			var parameter = Expression.Parameter(typeof (IFlagSettingProvider));
			var methodCall = Expression.Call(methodInfo, parameter);
			Contract.Assume(methodCall != null);

			var lambda = Expression.Lambda<Func<IFlagSettingProvider, FlagSetting<T>>>(
				methodCall,
				parameter);

			var getFlagSetting = lambda.Compile();

			Contract.Assume(getFlagSetting != null);

			return getFlagSetting;
		}

		// T is constrained correctly here, event though it looks a little odd
		// this wrapping methods allows us to use linq expressions, 
		// as they don't handle inheritance
		private static FlagSetting<T> GetFlagSetting<TFlag>(
			IFlagSettingProvider flagSettingProvider)
			where TFlag : FlagBase<T>
		{
			Contract.Requires(flagSettingProvider != null);

			return flagSettingProvider.Get<T, TFlag>();
		}

		[ContractInvariantMethod]
		[ExcludeFromCodeCoverage]
		private void ObjectInvariant()
		{
			Contract.Invariant(getFlagSetting != null);
		}

		#endregion
	}
}