﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Diagnostics.Contracts;
using System.Linq;

namespace FlagNET
{
	[ContractClass(typeof (IClass1Contract))]
	internal interface IClass1
	{

	}

	#region Contract class

	// ReSharper disable once InconsistentNaming
	[ContractClassFor(typeof(IClass1))]
	[ExcludeFromCodeCoverage]
	internal abstract class IClass1Contract : IClass1
	{

	}

	#endregion

	internal class Class1 : IClass1
	{
		#region Fields

		#endregion

		#region Constructors

		#endregion

		#region Properties

		#endregion

		#region Methods

		[ContractInvariantMethod]
		[ExcludeFromCodeCoverage]
		private void ObjectInvariant()
		{
		}

		#endregion
	}
}
