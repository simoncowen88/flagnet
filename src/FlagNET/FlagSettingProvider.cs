﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Diagnostics.Contracts;

namespace FlagNET
{
	/// <summary>
	/// Provides methods for getting strongly-typed settings for flags.
	/// </summary>
	public sealed class FlagSettingProvider : IFlagSettingProvider
	{
		#region Fields

		private readonly IFlagSettingReader flagSettingReader;
		private readonly IFlagSettingValueParser flagSettingValueParser;
		private readonly IFlagKeyProvider flagKeyProvider;

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="FlagSettingProvider"/> class.
		/// </summary>
		/// <param name="flagKeyProvider">The flag key provider.</param>
		/// <param name="flagSettingReader">The flag setting reader.</param>
		/// <param name="flagSettingValueParser">The flag setting value parser.</param>
		public FlagSettingProvider(
			IFlagKeyProvider flagKeyProvider,
			IFlagSettingReader flagSettingReader,
			IFlagSettingValueParser flagSettingValueParser)
		{
			Contract.Requires(flagKeyProvider != null);
			Contract.Requires(flagSettingReader != null);
			Contract.Requires(flagSettingValueParser != null);
			Assertion.ArgumentNotNull(() => flagKeyProvider);
			Assertion.ArgumentNotNull(() => flagSettingReader);
			Assertion.ArgumentNotNull(() => flagSettingValueParser);

			this.flagKeyProvider = flagKeyProvider;
			this.flagSettingReader = flagSettingReader;
			this.flagSettingValueParser = flagSettingValueParser;
		}

		#endregion

		#region Properties

		#endregion

		#region Methods

		/// <summary>
		/// Gets the setting for the given TFlag.
		/// </summary>
		/// <typeparam name="T">The type of the setting.</typeparam>
		/// <typeparam name="TFlag">The type of flag.</typeparam>
		/// <returns>
		/// The requested setting.
		/// </returns>
		public FlagSetting<T> Get<T, TFlag>()
			where TFlag : FlagBase<T>
		{
			Assertion.IsNotGeneric<TFlag>();

			var flagKey = flagKeyProvider.GetKey<TFlag>();

			string setting;
			var settingFound = flagSettingReader.TryRead(flagKey, out setting);

			if (!settingFound)
				return new FlagSetting<T>();

			var settingValue = flagSettingValueParser.Parse<T>(setting);
			return new FlagSetting<T>(settingValue);
		}

		[ContractInvariantMethod]
		[ExcludeFromCodeCoverage]
		private void ObjectInvariant()
		{
			Contract.Invariant(flagKeyProvider != null);
			Contract.Invariant(flagSettingReader != null);
			Contract.Invariant(flagSettingValueParser != null);
		}

		#endregion
	}
}