﻿using System.Collections.Generic;

namespace FlagNET
{
	/// <summary>
	/// Provides methods for getting settings by key.
	/// </summary>
	public interface IFlagSettingReader
	{
		/// <summary>
		/// Gets the setting associated with the specified key.
		/// </summary>
		/// <param name="key">The key whose value to get.</param>
		/// <param name="setting">
		/// The setting associated with the specified key, if the key is found;
		/// otherwise, null.
		/// </param>
		/// <returns>
		/// true if the specified key was found; otherwise, false.
		/// </returns>
		bool TryRead(string key, out string setting);
	}
}