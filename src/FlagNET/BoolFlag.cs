﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;

namespace FlagNET
{
	/// <summary>
	/// A base class for simple, boolean flags.
	/// </summary>
	[Pure]
	public abstract class BoolFlag : FlagBase<bool>
	{
		#region Fields

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="BoolFlag"/> class.
		/// </summary>
		protected BoolFlag()
		{
		}

		#endregion

		#region Properties

		#endregion

		#region Methods

		/// <summary>
		/// Determines whether this flag is enabled, given the specified setting.
		/// </summary>
		/// <param name="setting">The setting for the flag.</param>
		/// <returns>
		/// The given setting.
		/// </returns>
		protected override bool IsOn(bool setting)
		{
			return setting;
		}

		#endregion
	}
}