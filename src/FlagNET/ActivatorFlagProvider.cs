﻿using System;

namespace FlagNET
{
	/// <summary>
	/// Provides methods for creating instances of specific flags.
	/// This provider simply uses Activator.CreateInstance and is likely
	/// insufficient for production use.
	/// </summary>
	public sealed class ActivatorFlagProvider : IFlagProvider
	{
		#region Fields

		#endregion

		#region Constructors
		
		#endregion

		#region Properties

		#endregion

		#region Methods

		/// <summary>
		/// Gets an instance of the given TFlag.
		/// </summary>
		/// <typeparam name="TFlag">The type of flag requested.</typeparam>
		/// <returns>
		/// The requested flag instance.
		/// </returns>
		public TFlag Get<TFlag>() 
			where TFlag : FlagBase
		{
			return Activator.CreateInstance<TFlag>();
		}
		
		#endregion
	}
}