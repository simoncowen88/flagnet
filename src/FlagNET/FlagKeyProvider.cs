﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Diagnostics.Contracts;
using System.Linq;

namespace FlagNET
{
	/// <summary>
	/// Provides methods for getting keys for flag types.
	/// </summary>
	[ContractClass(typeof(IFlagKeyProviderContract))]
	public interface IFlagKeyProvider
	{
		/// <summary>Gets the key for the given flag type.</summary>
		/// <typeparam name="TFlag">The type of the flag.</typeparam>
		/// <returns>The key for the given flag type.</returns>
		string GetKey<TFlag>()
			where TFlag : FlagBase;
	}

	#region Contract class

	// ReSharper disable once InconsistentNaming
	[ContractClassFor(typeof(IFlagKeyProvider))]
	[ExcludeFromCodeCoverage]
	internal abstract class IFlagKeyProviderContract : IFlagKeyProvider
	{
		public string GetKey<TFlag>() where TFlag : FlagBase
		{
			Contract.Ensures(Contract.Result<string>() != null);

			return default (string);
		}
	}

	#endregion

	/// <summary>
	/// Provides methods for getting keys for flag types.
	/// This provider uses the namespace-qualified name of the flag type as the key.
	/// </summary>
	[Pure]
	public class FlagKeyProvider : IFlagKeyProvider
	{
		#region Fields

		#endregion

		#region Constructors

		#endregion

		#region Properties

		#endregion

		#region Methods

		/// <summary>Gets the key for the given flag type.</summary>
		/// <typeparam name="TFlag">The type of the flag.</typeparam>
		/// <returns>The key for the given flag type.</returns>
		public string GetKey<TFlag>()
			where TFlag : FlagBase
		{
			Assertion.IsNotGeneric<TFlag>();

			var flagType = typeof (TFlag);
			var flagKey = flagType.FullName;
			return flagKey;
		}

		#endregion
	}
}
