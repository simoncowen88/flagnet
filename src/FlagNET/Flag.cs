﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Diagnostics.Contracts;
using System.Linq;

namespace FlagNET
{
	/// <summary>
	/// Provides methods for reading the state of flags.
	/// </summary>
	[ContractClass(typeof(IFlagContract))]
	public interface IFlag
	{
		/// <summary>
		/// Determines whether the flag of the given type is enabled.
		/// </summary>
		/// <typeparam name="TFlag">The type of the flag.</typeparam>
		/// <returns>true, if the flag is enabled; otherwise, false.</returns>
		bool IsOn<TFlag>()
			where TFlag : FlagBase;
	}

	#region Contract class

	// ReSharper disable once InconsistentNaming
	[ContractClassFor(typeof(IFlag))]
	[ExcludeFromCodeCoverage]
	internal abstract class IFlagContract : IFlag
	{
		public abstract bool IsOn<TFlag>() 
			where TFlag : FlagBase;
	}

	#endregion

	/// <summary>
	/// Provides methods for reading the state of flags.
	/// </summary>
	[Pure]
	public sealed class Flag : IFlag
	{
		#region Fields

		private readonly IFlagSettingProvider flagSettingProvider;
		private readonly IFlagProvider flagProvider;

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="Flag"/> class.
		/// </summary>
		/// <param name="flagSettingProvider">The flag setting provider.</param>
		/// <param name="flagProvider">The flag provider.</param>
		public Flag(
			IFlagSettingProvider flagSettingProvider,
			IFlagProvider flagProvider)
		{
			Contract.Requires(flagSettingProvider != null);
			Contract.Requires(flagProvider != null);

			Assertion.ArgumentNotNull(() => flagSettingProvider);
			Assertion.ArgumentNotNull(() => flagProvider);

			this.flagSettingProvider = flagSettingProvider;
			this.flagProvider = flagProvider;
		}

		#endregion

		#region Properties

		#endregion

		#region Methods

		/// <summary>
		/// Determines whether the flag of the given type is enabled.
		/// </summary>
		/// <typeparam name="TFlag">The type of the flag.</typeparam>
		/// <returns>true, if the flag is enabled; otherwise, false.</returns>
		public bool IsOn<TFlag>() 
			where TFlag : FlagBase
		{
			Assertion.IsNotGeneric<TFlag>();

			var flag = flagProvider.Get<TFlag>();
			var isOn = flag.IsOn(flagSettingProvider);
			return isOn;
		}

		[ContractInvariantMethod]
		[ExcludeFromCodeCoverage]
		private void ObjectInvariant()
		{
			Contract.Invariant(flagSettingProvider != null);
			Contract.Invariant(flagProvider != null);
		}

		#endregion
	}
}
