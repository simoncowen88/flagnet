﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Diagnostics.Contracts;
using System.Linq;

namespace FlagNET
{
	/// <summary>
	/// Represents a setting value for a flag.
	/// </summary>
	/// <typeparam name="T">
	/// The type of the setting value.
	/// </typeparam>
	[Pure]
	public sealed class FlagSetting<T>
	{
		#region Fields

		private readonly T value = default (T);
		private readonly bool missing = false;

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="FlagSetting{T}"/> class 
		/// as a missing setting.
		/// </summary>
		public FlagSetting()
		{
			missing = true;
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="FlagSetting{T}"/> class 
		/// with the given value.
		/// </summary>
		/// <param name="value">The value of the setting.</param>
		public FlagSetting(T value)
		{
			this.value = value;
		}
		
		#endregion

		#region Properties

		#endregion

		#region Methods

		/// <summary>
		/// Applies the specified function to the setting value.
		/// </summary>
		/// <param name="func">The function to apply.</param>
		/// <returns>
		/// The result of the function applied to the setting value, 
		/// if the setting is not missing (i.e. has a value); otherwise, false.
		/// </returns>
		public bool Apply(Func<T, bool> func)
		{
			Contract.Requires(func != null);
			Assertion.ArgumentNotNull(() => func);

			if (missing)
				return false;

			return func(value);
		}

		[ContractInvariantMethod]
		[ExcludeFromCodeCoverage]
		private void ObjectInvariant()
		{
			Contract.Invariant(
				(missing && Equals(value, default(T)))
				||
				(!missing));
		}

		#endregion
	}
}
