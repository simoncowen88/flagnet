﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

namespace FlagNET
{
	/// <summary>
	/// Provides methods for getting settings by key.
	/// This provider reads the AppSettings section of the current application's
	/// default configuration.
	/// </summary>
	public sealed class AppConfigFlagSettingReader : IFlagSettingReader
	{
		#region Fields

		#endregion

		#region Constructors

		#endregion

		#region Properties

		#endregion

		#region Methods

		/// <summary>
		/// Gets the setting associated with the specified key.
		/// </summary>
		/// <param name="key">The key whose value to get.</param>
		/// <param name="setting">
		/// The setting associated with the specified key, if the key is found;
		/// otherwise, null.
		/// </param>
		/// <returns>
		/// true if the specified key was found; otherwise, false.
		/// </returns>
		public bool TryRead(string key, out string setting)
		{
			setting = ConfigurationManager.AppSettings[key];
			return setting != null;
		}

		#endregion
	}
}
