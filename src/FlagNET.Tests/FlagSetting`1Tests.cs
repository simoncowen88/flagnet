﻿using System;
using NUnit.Framework;

namespace FlagNET.Tests
{
	[TestFixture]
	public class FlagSettingOfTTests
	{
		#region Helper

		public abstract class TestBase
		{
			[SetUp]
			public virtual void SetUp()
			{
			}

			[TearDown]
			public virtual void TearDown()
			{
			}

			internal FlagSetting<T> GetTarget<T>()
			{
				return new FlagSetting<T>();
			}

			internal FlagSetting<T> GetTarget<T>(T value)
			{
				return new FlagSetting<T>(value);
			}
		}

		#endregion

		[TestFixture]
		public class ConstructorTests //: TestBase
		{
			[Test]
			public void can_be_created_with_missing_value()
			{
				var target = new FlagSetting<int>();
				Assert.Pass();
			}

			[Test]
			public void can_be_created_with_value_int()
			{
				var target = new FlagSetting<int>(3);
				Assert.Pass();
			}

			[Test]
			public void can_be_created_with_value_string()
			{
				var target = new FlagSetting<string>("foo");
				Assert.Pass();
			}

			[Test]
			public void can_be_created_with_value_string_null()
			{
				var target = new FlagSetting<string>(null);
				Assert.Pass();
			}
		}

		[TestFixture]
		public class ApplyTests : TestBase
		{
			[Test]
			public void when_setting_missing_null_func_throws()
			{
				var target = GetTarget<int>();

				Assert.That(
					() => target.Apply(null),
					Throws.Exception);
			}

			[Test]
			public void when_setting_missing_returns_false_and_doesnt_invoke_func()
			{
				var target = GetTarget<int>();
				const bool expected = false;
				var actual = target.Apply(
					v =>
					{
						Assert.Fail();
						return true;
					});

				Assert.That(actual, Is.EqualTo(expected));
			}

			[Test]
			public void null_func_throws()
			{
				var target = GetTarget<int>(7);

				Assert.That(
					() => target.Apply(null),
					Throws.Exception);
			}

			[TestCase(true)]
			[TestCase(false)]
			public void returns_result_of_func(bool expected)
			{
				var target = GetTarget<int>(7);

				var funcInvoked = false;
				var actual = target.Apply(
					v =>
					{
						funcInvoked = true;
						return expected;
					});

				Assert.That(actual, Is.EqualTo(expected));
				Assert.That(funcInvoked, Is.True);
			}
		}
	}
}
