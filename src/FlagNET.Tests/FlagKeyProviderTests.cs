﻿using System;
using NUnit.Framework;

namespace FlagNET.Tests
{
	internal class TestFlagNotNested : BoolFlag
	{
	}

	[TestFixture]
	public class FlagKeyProviderTests
	{
		#region Helper

		public abstract class TestBase
		{
			[SetUp]
			public virtual void SetUp()
			{
			}

			[TearDown]
			public virtual void TearDown()
			{
			}

			internal FlagKeyProvider GetTarget()
			{
				return new FlagKeyProvider();
			}
		}

		internal class TestFlagNested : BoolFlag
		{
		}

		#endregion

		[TestFixture]
		public class ConstructorTests //: TestBase
		{
			[Test]
			public void can_be_created()
			{
				var target = new FlagKeyProvider();
				Assert.Pass();
			}
		}

		[TestFixture]
		public class GetKeyTests : TestBase
		{
			internal class TestFlagNestedNested : BoolFlag
			{
			}

			internal class GenericFlag<T> : BoolFlag
			{
			}

			[Test]
			public void when_type_simple_is_correct()
			{
				var target = GetTarget();

				var actual = target.GetKey<TestFlagNotNested>();
				const string expected = "FlagNET.Tests.TestFlagNotNested";

				Assert.That(actual, Is.EqualTo(expected));
			}
			
			[Test]
			public void when_type_nested_is_correct()
			{
				var target = GetTarget();

				var actual = target.GetKey<TestFlagNested>();
				const string expected = "FlagNET.Tests.FlagKeyProviderTests+TestFlagNested";

				Assert.That(actual, Is.EqualTo(expected));
			}
			
			[Test]
			public void when_type_double_nested_is_correct()
			{
				var target = GetTarget();

				var actual = target.GetKey<TestFlagNestedNested>();
				const string expected = "FlagNET.Tests.FlagKeyProviderTests+GetKeyTests+TestFlagNestedNested";

				Assert.That(actual, Is.EqualTo(expected));
			}
			
			[Test]
			public void when_flag_generic_throws()
			{
				var target = GetTarget();

				Assert.That(
					() => target.GetKey<GenericFlag<int>>(),
					Throws.Exception);
			}
		}
	}
}
