﻿using System;
using NUnit.Framework;

namespace FlagNET.Tests
{
	[TestFixture]
	public class SettingValueParserTests
	{
		#region Helper

		public abstract class TestBase
		{
			[SetUp]
			public virtual void SetUp()
			{
			}

			[TearDown]
			public virtual void TearDown()
			{
			}

			internal FlagSettingValueParser GetTarget()
			{
				return new FlagSettingValueParser();
			}
		}

		#endregion

		[TestFixture]
		public class ConstructorTests //: TestBase
		{
			[Test]
			public void can_be_created()
			{
				var target = new FlagSettingValueParser();
				Assert.Pass();
			}
		}

		[TestFixture]
		public class ParseTests : TestBase
		{
			private void AssertWorks<T>(T expected, string valueString)
			{
				var target = GetTarget();
				var actual = target.Parse<T>(valueString);

				Assert.That(actual, Is.EqualTo(expected));
			}
			private void AssertThrows<T>(string valueString)
			{
				var target = GetTarget();

				Assert.That(
					() => target.Parse<T>(valueString),
					Throws.ArgumentException);
			}

			private class UnparsableClass
			{
			}

			[Test]
			public void when_unknown_type_throws()
			{
				var target = GetTarget();

				var exception = Assert.Throws<Exception>(
					() => target.Parse<UnparsableClass>("test"));
			}

			[Test]
			public void when_not_parsable_throws()
			{
				AssertThrows<int>("");
				AssertThrows<int>("test");
				AssertThrows<int>("7.6");
			}

			[Test]
			public void works_for_various_types()
			{
				AssertWorks<int>(7, "7");
				AssertWorks<int?>(7, "7");
				AssertWorks<int?>(null, "null");
				AssertWorks<int?>(null, null);

				AssertWorks<double>(7d, "7");
				AssertWorks<double?>(7d, "7");
				AssertWorks<double?>(null, "null");
				AssertWorks<double?>(null, null);

				AssertWorks<string>("foo", "foo");
				AssertWorks<string>("null", "null");
				AssertWorks<string>(null, null);

				AssertWorks<int[]>(new int[] { 1, 2, 3 }, "1,2,3");
				AssertWorks<int[]>(new int[0], "");
				AssertWorks<int[]>(null, null);

				AssertWorks<int?[]>(new int?[] { 1, 2, 3 }, "1,2,3");
				AssertWorks<int?[]>(new int?[0], "");
				AssertWorks<int?[]>(new int?[] { 1, null, 3 }, "1,null,3");
				AssertWorks<int?[]>(null, null);

				// cover all the bases

				AssertWorks<bool>(true, "true");
				AssertWorks<byte>(7, "7");
				AssertWorks<short>(7, "7");
				AssertWorks<int>(7, "7");
				AssertWorks<long>(7, "7");

				AssertWorks<float>(7f, "7");
				AssertWorks<double>(7d, "7");

				AssertWorks<decimal>(7m, "7");

				AssertWorks<string>("test", "test");

				var guid = Guid.NewGuid();
				AssertWorks<Guid>(guid, guid.ToString());

				AssertWorks<char>('x', "x");

				var dateTime = DateTime.Now;
				AssertWorks<DateTime>(dateTime, dateTime.ToString("o"));

				var dateTimeOffset = DateTimeOffset.Now;
				AssertWorks<DateTimeOffset>(dateTimeOffset, dateTimeOffset.ToString("o"));

				var timeSpan = TimeSpan.FromSeconds(3.4);
				AssertWorks<TimeSpan>(timeSpan, timeSpan.ToString());

				AssertWorks<sbyte>(7, "7");
				AssertWorks<uint>(7, "7");
				AssertWorks<ulong>(7, "7");
				AssertWorks<ushort>(7, "7");
			}
		}
	}
}
