﻿using System;
using System.Linq.Expressions;
using NUnit.Framework;

namespace FlagNET.Tests
{
	[TestFixture]
	public class AsssertionTests
	{
		#region Helper

		public abstract class TestBase
		{
			[SetUp]
			public virtual void SetUp()
			{
			}

			[TearDown]
			public virtual void TearDown()
			{
			}
		}

		#endregion

		[TestFixture]
		public class ArgumentNotNullTests : TestBase
		{
			private string testField;
			private string TestProperty { get; set; }
			private void TestMethod(string testParameter)
			{
				Assertion.ArgumentNotNull(() => testParameter);
			}
			private void TestMethodExpressionVariable(string testParameter)
			{
				string testVariable = testParameter;
				Assertion.ArgumentNotNull(() => testVariable);
			}

			[Test]
			public void when_expression_is_parameter_null_throws()
			{
				var exception = Assert.Throws<ArgumentNullException>(
					() => TestMethod(null));

				Assert.That(exception.ParamName, Is.EqualTo("testParameter"));
			}

			[Test]
			public void when_expression_is_parameter_not_null_does_nothing()
			{
				Assert.That(
					() => TestMethod(""),
					Throws.Nothing);
			}

			[Test]
			public void when_expression_is_variable_null_throws()
			{
				var exception = Assert.Throws<ArgumentNullException>(
					() => TestMethodExpressionVariable(null));

				Assert.That(exception.ParamName, Is.EqualTo("testVariable"));
			}

			[Test]
			public void when_expression_is_variable_not_null_does_nothing()
			{
				Assert.That(
					() => TestMethodExpressionVariable(""),
					Throws.Nothing);
			}

			[Test]
			public void when_expression_is_field_null_throws()
			{
				testField = null;
				var exception = Assert.Throws<ArgumentNullException>(
					() => Assertion.ArgumentNotNull(() => testField));

				Assert.That(exception.ParamName, Is.EqualTo("testField"));
			}

			[Test]
			public void when_expression_is_field_not_null_does_nothing()
			{
				testField = "";
				Assert.That(
					() => Assertion.ArgumentNotNull(() => testField),
					Throws.Nothing);
			}

			[Test]
			public void when_expression_is_property_null_throws()
			{
				TestProperty = null;
				var exception = Assert.Throws<ArgumentNullException>(
					() => Assertion.ArgumentNotNull(() => TestProperty));

				Assert.That(exception.ParamName, Is.EqualTo("TestProperty"));
			}

			[Test]
			public void when_expression_is_property_not_null_does_nothing()
			{
				TestProperty = "";
				Assert.That(
					() => Assertion.ArgumentNotNull(() => TestProperty),
					Throws.Nothing);
			}


			[Test]
			public void when_expression_is_null_throws()
			{
				Expression<Func<string>> expression = null;

				var exception = Assert.Throws<ArgumentNullException>(
					() => Assertion.ArgumentNotNull(expression));

				Assert.That(exception.ParamName, Is.EqualTo("expression"));
			}

			[Test]
			public void when_expression_is_constant_null_throws()
			{
				var exception = Assert.Throws<ArgumentException>(
					() => Assertion.ArgumentNotNull(() => (string)null));

				Assert.That(exception.ParamName, Is.EqualTo("expression"));
			}

			[Test]
			public void when_expression_is_constant_not_null_throws()
			{
				var exception = Assert.Throws<ArgumentException>(
					() => Assertion.ArgumentNotNull(() => ""));

				Assert.That(exception.ParamName, Is.EqualTo("expression"));
			}
		}
	}
}