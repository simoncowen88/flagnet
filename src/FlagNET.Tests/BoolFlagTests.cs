﻿using System;
using Moq;
using NUnit.Framework;

namespace FlagNET.Tests
{
	[TestFixture]
	public class BoolFlagTests
	{
		#region Helper

		public abstract class TestBase
		{
			internal Mock<IFlagSettingProvider> mockFlagSettingProvider;

			[SetUp]
			public virtual void SetUp()
			{
				mockFlagSettingProvider = new Mock<IFlagSettingProvider>();
			}

			[TearDown]
			public virtual void TearDown()
			{
			}

			internal TestFlag GetTarget(Action<bool> isOnCallback)
			{
				return new TestFlag(isOnCallback);
			}
		}

		internal class TestFlag : BoolFlag
		{
			private readonly Action<bool> isOnCallback;

			public TestFlag(Action<bool> isOnCallback)
			{
				this.isOnCallback = isOnCallback;
			}

			protected override bool IsOn(bool setting)
			{
				isOnCallback(setting);
				return base.IsOn(setting);
			}
		}

		#endregion

		[TestFixture]
		public class ConstructorTests //: TestBase
		{
			[Test]
			public void can_be_created()
			{
				var target = new TestFlag(b => { });
				Assert.Pass();
			}
		}

		[TestFixture]
		public class IsOnTests : TestBase
		{
			[TestCase(true)]
			[TestCase(false)]
			public void when_setting_present_returns_setting(bool setting)
			{
				mockFlagSettingProvider
					   .Setup(o => o.Get<bool, TestFlag>())
					   .Returns(new FlagSetting<bool>(setting));

				var callbackInvoked = false;
				var target = GetTarget(
					b =>
					{
						callbackInvoked = true;
						Assert.That(b, Is.EqualTo(setting));
					});

				var expected = setting;
				var actual = target.IsOn(mockFlagSettingProvider.Object);

				Assert.That(callbackInvoked, Is.True);
				Assert.That(actual, Is.EqualTo(expected));
			}

			[Test]
			public void when_setting_missing_returns_false_if()
			{
				mockFlagSettingProvider
					   .Setup(o => o.Get<bool, TestFlag>())
					   .Returns(new FlagSetting<bool>());

				var target = GetTarget(b => Assert.Fail());

				const bool expected = false;
				var actual = target.IsOn(mockFlagSettingProvider.Object);

				Assert.That(actual, Is.EqualTo(expected));
			}
		}
	}
}
