﻿using System;
using NUnit.Framework;

namespace FlagNET.Tests
{
	[TestFixture]
	public class Class1Tests
	{
		#region Helper

		public abstract class TestBase
		{
			[SetUp]
			public virtual void SetUp()
			{
			}

			[TearDown]
			public virtual void TearDown()
			{
			}

			internal Class1 GetTarget()
			{
				return new Class1();
			}
		}

		#endregion

		[TestFixture]
		public class ConstructorTests //: TestBase
		{
			[Test]
			public void can_be_created()
			{
			}
		}

		[TestFixture]
		public class Method1Tests : TestBase
		{
			[Test]
			public void doesnt_throw()
			{
			}
		}
	}
}
