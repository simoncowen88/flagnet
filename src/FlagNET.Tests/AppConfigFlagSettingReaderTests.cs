﻿using System;
using System.Configuration;
using NUnit.Framework;

namespace FlagNET.Tests
{
	[TestFixture]
	public class AppConfigFlagSettingReaderTests
	{
		#region Helper

		public abstract class TestBase
		{
			[SetUp]
			public virtual void SetUp()
			{
			}

			[TearDown]
			public virtual void TearDown()
			{
			}

			internal AppConfigFlagSettingReader GetTarget()
			{
				return new AppConfigFlagSettingReader();
			}
		}

		#endregion

		[TestFixture]
		public class ConstructorTests //: TestBase
		{
			[Test]
			public void can_be_created()
			{
				var target = new AppConfigFlagSettingReader();
				Assert.Pass();
			}
		}

		[TestFixture]
		public class TryReadTests : TestBase
		{
			[Test]
			public void when_setting_missing_returns_false()
			{
				var target = GetTarget();

				string actualSetting;
				var actualResult = target.TryRead("ThisWontBeThere", out actualSetting);

				Assert.That(actualResult, Is.False);
				Assert.That(actualSetting, Is.Null);
			}

			[Test]
			public void when_setting_present_returns_true()
			{
				const string key = "ThisWillBeThere";
				const string setting = "testing";

				ConfigurationManager.AppSettings[key] = setting;

				var target = GetTarget();

				string actualSetting;
				var actualResult = target.TryRead(key, out actualSetting);

				Assert.That(actualResult, Is.True);
				Assert.That(actualSetting, Is.EqualTo(setting));
			}
		}
	}
}