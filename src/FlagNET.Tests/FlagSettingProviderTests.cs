﻿using System;
using Moq;
using NUnit.Framework;

namespace FlagNET.Tests
{
	[TestFixture]
	public class FlagSettingProviderTests
	{
		#region Helper

		public abstract class TestBase
		{
			internal Mock<IFlagSettingReader> mockFlagSettingReader;
			internal Mock<IFlagSettingValueParser> mockSettingValueParser;
			internal Mock<IFlagKeyProvider>  mockFlagKeyProvider;

			[SetUp]
			public virtual void SetUp()
			{
				mockFlagKeyProvider = new Mock<IFlagKeyProvider>();
				mockFlagSettingReader = new Mock<IFlagSettingReader>();
				mockSettingValueParser = new Mock<IFlagSettingValueParser>();
			}

			[TearDown]
			public virtual void TearDown()
			{
			}

			internal FlagSettingProvider GetTarget()
			{
				return new FlagSettingProvider(
					mockFlagKeyProvider.Object,
					mockFlagSettingReader.Object,
					mockSettingValueParser.Object);
			}
		}

		#endregion

		[TestFixture]
		public class ConstructorTests //: TestBase
		{
			[Test]
			public void can_be_created()
			{
				var mockFlagKeyProvider = new Mock<IFlagKeyProvider>();
				var mockFlagSettingReader = new Mock<IFlagSettingReader>();
				var mockFlagSettingValueParser = new Mock<IFlagSettingValueParser>();

				var target = new FlagSettingProvider(
					mockFlagKeyProvider.Object,
					mockFlagSettingReader.Object,
					mockFlagSettingValueParser.Object);
				Assert.Pass();
			}

			[Test]
			public void when_flagKeyProvider_null_throws()
			{
				var mockFlagSettingReader = new Mock<IFlagSettingReader>();
				var mockFlagSettingValueParser = new Mock<IFlagSettingValueParser>();

				Assert.That(
					() => new FlagSettingProvider(null, mockFlagSettingReader.Object, mockFlagSettingValueParser.Object),
					Throws.Exception);
			}

			[Test]
			public void when_flagSettingReader_null_throws()
			{
				var mockFlagKeyProvider = new Mock<IFlagKeyProvider>();
				var mockFlagSettingValueParser = new Mock<IFlagSettingValueParser>();

				Assert.That(
					() => new FlagSettingProvider(mockFlagKeyProvider.Object, null, mockFlagSettingValueParser.Object),
					Throws.Exception);
			}

			[Test]
			public void when_flagSettingValueParser_null_throws()
			{
				var mockFlagKeyProvider = new Mock<IFlagKeyProvider>();
				var mockFlagSettingReader = new Mock<IFlagSettingReader>();

				Assert.That(
					() => new FlagSettingProvider(mockFlagKeyProvider.Object, mockFlagSettingReader.Object, null),
					Throws.Exception);
			}
		}

		[TestFixture]
		public class GetTests : TestBase
		{
			private class TestFlag : BoolFlag
			{
			}

			private class GenericFlag<T> : BoolFlag
			{
			}

			private void AssertIsMissingValue<T>(FlagSetting<T> flagSetting)
			{
				var result = flagSetting.Apply(
					o =>
					{
						// is the value is missing, this will never be invoked
						Assert.Fail();
						throw new AssertionException("");
					});
			}
			private void AssertHasValue<T>(FlagSetting<T> flagSetting, T expected)
			{
				var invoked = false;
				var actual = default (T);
				var result = flagSetting.Apply(
					o =>
					{
						invoked = true;
						actual = o;

						return true;
					});

				Assert.That(invoked, Is.True);
				Assert.That(actual, Is.EqualTo(expected));
			}

			[Test]
			public void when_setting_missing_returns_missing_setting()
			{
				const string flagKey = "key";
				mockFlagKeyProvider
					.Setup(o => o.GetKey<TestFlag>())
					.Returns(flagKey);

				var setting = "settingValue";
				mockFlagSettingReader
					.Setup(o => o.TryRead(flagKey, out setting))
					.Returns(false);

				var target = GetTarget();

				var actual = target.Get<bool, TestFlag>();
				AssertIsMissingValue(actual);
			}

			[TestCase(true)]
			[TestCase(false)]
			public void parses_found_setting_and_returns_correct_value(bool parsedValue)
			{
				const string flagKey = "key";
				mockFlagKeyProvider
					.Setup(o => o.GetKey<TestFlag>())
					.Returns(flagKey);

				var setting = "settingValue";
				mockFlagSettingReader
					.Setup(o => o.TryRead(flagKey, out setting))
					.Returns(true);
				mockSettingValueParser
					.Setup(o => o.Parse<bool>("settingValue"))
					.Returns(parsedValue);

				var target = GetTarget();

				var actual = target.Get<bool, TestFlag>();
				AssertHasValue(actual, parsedValue);
			}

			[Test]
			public void when_flag_generic_throws()
			{
				var target = GetTarget();

				Assert.That(
					() => target.Get<bool, GenericFlag<int>>(),
					Throws.Exception);
			}
		}
	}
}
