﻿using System;
using Moq;
using NUnit.Framework;

namespace FlagNET.Tests
{
	[TestFixture]
	public class FlagReaderTests
	{
		#region Helper

		public abstract class TestBase
		{
			internal Mock<IFlagSettingProvider> mockFlagSettingProvider;
			internal Mock<IFlagProvider> mockFlagProvider;

			[SetUp]
			public virtual void SetUp()
			{
				mockFlagSettingProvider = new Mock<IFlagSettingProvider>();
				mockFlagProvider = new Mock<IFlagProvider>();

				mockFlagProvider
					.Setup(o => o.Get<TestFlag>())
					.Returns(new TestFlag());
			}

			[TearDown]
			public virtual void TearDown()
			{
			}

			internal Flag GetTarget()
			{
				return new Flag(
					mockFlagSettingProvider.Object,
					mockFlagProvider.Object);
			}
		}

		private class TestFlag : FlagBase<bool>
		{
			protected override bool IsOn(bool setting)
			{
				return setting;
			}
		}

		private class GenericFlag<T> : BoolFlag
		{
		}

		#endregion

		[TestFixture]
		public class ConstructorTests //: TestBase
		{
			[Test]
			public void can_be_created()
			{
				var mockFlagSettingProvider = new Mock<IFlagSettingProvider>();
				var mockFlagProvider = new Mock<IFlagProvider>();

				var target = new Flag(
					mockFlagSettingProvider.Object,
					mockFlagProvider.Object);

				Assert.Pass();
			}

			[Test]
			public void when_flagSettingProvider_null_throws()
			{
				var mockFlagProvider = new Mock<IFlagProvider>();

				Assert.That(
					() => new Flag(null, mockFlagProvider.Object),
					Throws.Exception);
			}

			[Test]
			public void when_flagProvider_null_throws()
			{
				var mockFlagSettingProvider = new Mock<IFlagSettingProvider>();

				Assert.That(
					() => new Flag(mockFlagSettingProvider.Object, null),
					Throws.Exception);
			}
		}

		[TestFixture]
		public class IsOnOfTTests : TestBase
		{
			[TestCase(true)]
			[TestCase(false)]
			public void when_setting_present_handles_setting_correctly(bool setting)
			{
				var flagSetting = new FlagSetting<bool>(setting);
				mockFlagSettingProvider
					   .Setup(o => o.Get<bool, TestFlag>())
					.Returns(flagSetting);

				var target = GetTarget();

				var expected = setting;
				var actual = target.IsOn<TestFlag>();

				Assert.That(actual, Is.EqualTo(expected));
			}

			[Test]
			public void when_setting_missing_returns_false()
			{
				mockFlagSettingProvider
					   .Setup(o => o.Get<bool, TestFlag>())
					   .Returns(new FlagSetting<bool>());

				var target = GetTarget();

				const bool expected = false;
				var actual = target.IsOn<TestFlag>();

				Assert.That(actual, Is.EqualTo(expected));

			}

			[Test]
			public void when_flag_generic_throws()
			{
				var target = GetTarget();

				Assert.That(
					() => target.IsOn<GenericFlag<int>>(),
					Throws.Exception);
			}
		}
	}
}
