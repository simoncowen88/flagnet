﻿using System;
using NUnit.Framework;

namespace FlagNET.Tests
{
	[TestFixture]
	public class ActivatorFlagProviderTests
	{
		#region Helper

		public abstract class TestBase
		{
			[SetUp]
			public virtual void SetUp()
			{
			}

			[TearDown]
			public virtual void TearDown()
			{
			}

			internal ActivatorFlagProvider GetTarget()
			{
				return new ActivatorFlagProvider();
			}
		}

		#endregion

		[TestFixture]
		public class ConstructorTests //: TestBase
		{
			[Test]
			public void can_be_created()
			{
				var target = new ActivatorFlagProvider();
				Assert.Pass();
			}
		}

		[TestFixture]
		public class GetTests : TestBase
		{
			private class TestFlag_ParameterlessConstructor : BoolFlag
			{ }

			private class TestFlag_ParameterConstructor : BoolFlag
			{
				public TestFlag_ParameterConstructor(int number)
				{
				}
			}

			[Test]
			public void when_flag_has_parameterless_constructor_can_create()
			{
				var target = GetTarget();
				var actual = target.Get<TestFlag_ParameterlessConstructor>();

				Assert.That(actual, Is.Not.Null);
				Assert.Pass();
			}

			[Test]
			public void when_flag_doesnt_have_parameterless_constructor_cannot_create()
			{
				var target = GetTarget();

				Assert.That(
					() =>target.Get<TestFlag_ParameterConstructor>(),
					Throws.Exception);

			}
		}
	}
}
